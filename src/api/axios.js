import axios from "axios"
import { BASE_URL } from "@/constants/api.constant"

import { useToast } from 'vue-toastification'
import { useLoading } from 'vue-loading-overlay'

const toast = useToast()
let loader = null

const instance = axios.create({
    baseURL : BASE_URL
})

instance.interceptors.request.use(function (config) {
    const loading = useLoading()
    loader = loading.show()
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

instance.interceptors.response.use(function (response) {
    toast.success('Success')
    loader.hide()
    return response;
  }, function (error) {
    toast.error(error.message)
    loader.hide()
    return Promise.reject(error);
  });

export default instance