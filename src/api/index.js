import { BASE_URL } from '@/constants/api.constant'
import api from './axios'

export const GET = async (path) => {
    try {
        const { data } = await api.get(`${BASE_URL}/${path}`)
        return data
    } catch (error) {
        return Promise.reject(error)
    }
}

export const POST = async (path, body) => {
    try {
        const { data } = await api.post(`${BASE_URL}/${path}`, body)
        return data
    } catch (error) {
        return Promise.reject(error)
    }
}