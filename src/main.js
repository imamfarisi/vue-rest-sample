import { createApp } from 'vue'
import App from './App.vue'

import router from '@/router'

// import store from './store'
import { createPinia } from 'pinia'

import '@/assets/main.css'

import { LoadingPlugin } from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/css/index.css'
    
import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'

const app = createApp(App)
app.use(router)

// app.use(store)
app.use(createPinia())

app.use(LoadingPlugin);
app.use(Toast)

app.mount('#app')
