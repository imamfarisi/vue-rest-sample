import { GET } from '@/api/index'

export const getUsers = () => GET(`users?page=1`)