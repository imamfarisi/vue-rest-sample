import { createStore } from "vuex"

import userStore from "@/store/user/user"

export default createStore({
    state: {
        isLoading: false
    },
    mutations: {
        setLoading(state) {
            state.isLoading = !state.isLoading
        }
    },
    getters: {
        getLoading({ isLoading }) {
            return isLoading
        }
    },
    modules: {
        user: userStore
    }
})

