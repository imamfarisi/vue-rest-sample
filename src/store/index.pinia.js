import { defineStore } from 'pinia'

export default defineStore('main-store', {
    state: () => {
        return {
            isLoading: false
        }
    },
    actions: {
        setLoading() {
            this.isLoading = !this.isLoading
        }
    },
    getters: {
        getLoading({ isLoading }) {
            return isLoading
        }
    }
})