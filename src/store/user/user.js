import { getUsers } from "@/services/rest/user.service"

export default {
    state: {
        data: []
    },
    mutations: {
        getUserMutation(state, payload) {
            state.data = payload
        }
    },
    actions: {
        async getUserAction({ commit }) {
            commit('setLoading')
            const { data } = await getUsers()
            commit('getUserMutation', data)
            commit('setLoading')
        }
    },
    getters: {
        getUserStore({ data }) {
            return data.map(d => {
                d.fullName = `${d.first_name} ${d.last_name}`
                return d
            })
        }
    }
}