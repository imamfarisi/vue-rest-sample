import { getUsers } from "@/services/rest/user.service"
import { defineStore } from 'pinia'

import mainStore from "@/store/index.pinia";

export default defineStore('user-store', {
    state: () => {
        return {
            data: []
        }
    },
    actions: {
        async getUserAction() {
            const mStore = mainStore()
            mStore.setLoading()
            try {
                const { data } = await getUsers()
                this.data = data
            } catch (err) { }
            mStore.setLoading()
        }
    },
    getters: {
        getUserStore({ data }) {
            return data.map(d => {
                d.fullName = `${d.first_name} ${d.last_name}`
                return d
            })
        }
    }
})